from flask import jsonify, request
from app.models.anime_model import AnimeModel
from app.exceptions.exec import InvalidKeys
from psycopg2.errors import UniqueViolation


def update_anime(anime_id):

    data = request.get_json()

    try:
        AnimeModel.validate(**data)

        anime = AnimeModel.update_anime(anime_id, data)

        try:
            return jsonify(anime), 200
        except:
            return jsonify({'error': 'Not Found'}), 404
    except:
        invalid_anime = InvalidKeys(AnimeModel.validate(**data))
        return jsonify(invalid_anime.__dict__), 422


def delete_anime(anime_id):

    try:
        deleted_anime = AnimeModel.delete_anime(anime_id)
        return jsonify(deleted_anime), 204
    except:
        return jsonify({'error': 'Not Found'}), 404


def create_anime():

    data = request.get_json()

    try:

        AnimeModel.validate(**data)
        try:
            anime = AnimeModel(**data)

            return jsonify(anime.create_anime()), 201

        except UniqueViolation as err:
            return jsonify({'error': 'anime already exists'}), 409

    except:
        invalid_anime = InvalidKeys(AnimeModel.validate(**data))
        return jsonify(invalid_anime.__dict__), 422


def get_all():
    return jsonify(AnimeModel.get_all()), 200


def get_by_id(anime_id: int):
    try:
        return jsonify(AnimeModel.get_by_id(anime_id)), 200
    except:
        return jsonify({'error': 'Not Found'}), 404
