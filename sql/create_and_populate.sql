CREATE TABLE IF NOT EXISTS animes (
    id              SERIAL PRIMARY KEY,
    anime           VARCHAR(100) NOT NULL UNIQUE,
    released_date   DATE NOT NULL,
    seasons         INT NOT NULL
);


