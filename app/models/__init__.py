from dotenv import load_dotenv
from os import getenv
import psycopg2

load_dotenv()

configs = {
    "host": getenv("HOST"),
    "database": getenv("DB"),
    "user": getenv("USER"),
    "password": getenv("PASS")
}


def conn_cur():
    conn = psycopg2.connect(**configs)
    cur = conn.cursor()

    cur.execute("""
    CREATE TABLE IF NOT EXISTS animes (
        id              SERIAL PRIMARY KEY,
        anime           VARCHAR(100) NOT NULL UNIQUE,
        released_date   DATE NOT NULL,
        seasons         INT NOT NULL
);
    """)

    return conn, cur


def commit_and_close(conn, cur):

    conn.commit()
    cur.close()
    conn.close()
