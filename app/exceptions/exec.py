class ExistingAnimeError(Exception):

    def __init__(self):
        self.message = {'error': 'Anime already exists.'}

        super().__init__(self.message)


class InvalidKeys():

    available_keys = ["anime", "released_date", "seasons"]

    def __init__(self, missing_keys):
        self.error = {
            "available_keys": self.available_keys,
            "wrong_keys_sended": missing_keys}
