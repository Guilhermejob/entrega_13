from flask import Blueprint
from app.controllers.anime_controller import (
    get_all, get_by_id, create_anime, delete_anime, update_anime)


bp = Blueprint('bp_animes', __name__, url_prefix="/animes")


bp.get('')(get_all)

bp.get('/<int:anime_id>')(get_by_id)

bp.post('')(create_anime)

bp.delete('/<int:anime_id>')(delete_anime)

bp.patch('/<int:anime_id>')(update_anime)
