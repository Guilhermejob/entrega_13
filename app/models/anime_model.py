from . import conn_cur, commit_and_close
from psycopg2 import sql


class AnimeModel:

    anime_keys = ["id", "anime", "released_date", "seasons"]

    validate_keys = ["anime", "released_date", "seasons"]

    def __init__(self, anime, released_date, seasons):
        self.anime = anime.title()
        self.released_date = released_date
        self.seasons = seasons

    def validate(**kwargs):
        for key in AnimeModel.validate_keys:
            for item in kwargs:
                if item not in AnimeModel.validate_keys:
                    return item

        animes_list = AnimeModel.get_all()

        for key in animes_list['data']:
            if kwargs['anime'].lower() in key['anime'].lower():
                return 'O anime ja existe'

    @staticmethod
    def get_all():
        conn, cur = conn_cur()

        cur.execute("""
            SELECT  
                id, anime, to_char(released_date::DATE, 'DD/MM/YYYY'), seasons
            FROM 
                animes a
            ORDER BY a.id;
        """)

        animes = cur.fetchall()

        commit_and_close(conn, cur)

        data = {"data": []}

        animes_found = [dict(zip(AnimeModel.anime_keys, anime))
                        for anime in animes]

        for anime in animes_found:
            data['data'].append(anime)

        return data

    @staticmethod
    def get_by_id(id):
        conn, cur = conn_cur()

        cur.execute("""
            SELECT  
                id, anime, to_char(released_date::DATE, 'DD/MM/YYYY'), seasons
            FROM 
                animes a
            WHERE
                id = (%s)
        """, (id,))

        anime = cur.fetchone()

        commit_and_close(conn, cur)

        filtered_anime = dict(zip(AnimeModel.anime_keys, anime))

        data = {"data": filtered_anime}

        return data

    def create_anime(self):

        anime = self.__dict__

        conn, cur = conn_cur()

        query = """
            INSERT INTO
                animes
            (anime, released_date, seasons) 
            VALUES
                (%s, %s, %s)
            RETURNING * 
        """

        query_values = list(self.__dict__.values())

        cur.execute(query, query_values)

        inserted_data = cur.fetchone()

        commit_and_close(conn, cur)

        data = dict(zip(self.anime_keys, inserted_data))

        data['released_date'] = anime['released_date']

        return data

    @staticmethod
    def update_anime(anime_id, payload):
        payload['anime'] = payload['anime'].title()

        conn, cur = conn_cur()

        columns = [sql.Identifier(key) for key in payload.keys()]
        values = [sql.Literal(value) for value in payload.values()]

        query = sql.SQL(
            """
                UPDATE
                    animes
                SET
                    ({columns}) = row({values})
                WHERE
                    id = {id}
                RETURNING *;
            """
        ).format(
            id=sql.Literal(anime_id),
            columns=sql.SQL(',').join(columns),
            values=sql.SQL(',').join(values)
        )

        cur.execute(query)

        cur.execute("""
            SELECT  
                id, anime, to_char(released_date::DATE, 'DD/MM/YYYY'), seasons
            FROM 
                animes a
            WHERE
                id = (%s)
        """, (anime_id,))

        anime = cur.fetchone()

        commit_and_close(conn, cur)

        return dict(zip(AnimeModel.anime_keys, anime))

    @staticmethod
    def delete_anime(anime_id):
        conn, cur = conn_cur()

        query = """
            DELETE FROM 
                animes
            WHERE
                id = %s
            RETURNING *;
        """

        cur.execute(query, (anime_id,))

        deleted_anime = cur.fetchone()

        commit_and_close(conn, cur)

        return dict(zip(AnimeModel.anime_keys, deleted_anime))
